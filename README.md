# Info
Feelife is a service for offering people new types of activities. The idea of the project is as follows: some people have little free time and they want to explore new activities or try something new. Feelife will help them with this.

## Demo
[Video with demo](https://youtu.be/x1OpNd4iico)

You can visit [service's website](https://feelife-iu.web.app/) and start to use our application.

## Repositories
Source code of service divided into several repositories. Detailed explanation of each repository can be found on README files on repository's root directory. Complete list below:
1. [Backend](https://gitlab.com/feelife/backend)
2. [Frontend](https://gitlab.com/feelife/frontend)

## Requirements
Requirements and related information can be found on this [RUP document](https://docs.google.com/document/d/1lK5Ovbfl36nB60_8Oob1AXnfj2iL9id2/edit?usp=sharing&ouid=113719307258646054008&rtpof=true&sd=true).

## Design
### Database schema
![](diagrams/db_schema.png)

### Deployment diagram
![](diagrams/deployment_diagram.png)

### Class diagram
We recommend open this diagram using image viewer or open it in another tab for zooming small elements.
![](diagrams/class_diagram.png)

### Sequence diagram
![](diagrams/sequence_diagram.png)

### SOLID and Design patterns
Our developers actively used SOLID and several Design patterns.
For example, we have used Builder pattern, for example for HTTP requests.
Also, we have Middleware (or Decorator, in such case they behave very similar) for 
filtering incoming requests to protected endpoints.

## Architecture  
### Static view Diagram
![](diagrams/static_view.png)

### Dynamic view  diagram
![](diagrams/dynamic_view.png)

## CI/CD
We have used GitLab CI for our CI/CD pipelines.
Tests, builds and deploys are automated.
![](diagrams/ci_cd.png)

## Code

### BackEnd

#### Linter
We have used SonarLint plugin for IntelliJ IDEA.
![](diagrams/linter.png)

#### Test coverage
We have used JaCoCo for measuring test coverage.
![](diagrams/test_coverage.png)

### FrontEnd

#### Linter
We have used ESLint for identifying and reporting on patterns in JavaScript.\
In addition, Prettier was used to maintain a uniform code style.

#### Test coverage
We have used Jest library for writing tests and measuring test coverage.
![](diagrams/front_test_coverage.png)